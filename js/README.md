# Dicemate JavaScript

Simple HTML/CSS/JS example in a single file that works offline, easy to carry around and should work on almost any JavaScript supported browser.

The code has been kept in vanilla JavaScript and with as much support for old browsers as possible. No tools like Node.js, Gulp, Webpack are used in order to keep it simple. Everything is done the old fashion way. No cookies are recorded - the Customize form works through GET requests. There are no server side processing - everything is done client side. Only your browser has access to the generated passphrase.

The `wordlist` variable has been prepared with a simple bash command like:

```
sed -ne 's/^\(.*\)$/'\''\1\\n'\''+/p' eff_large_wordlist.txt > /tmp/wordlist.txt
```

Then `/tmp/wordlist.txt` is copied and edited appropriately.

## Usage

Just open the dicemate.html on a web browser by pressing Ctrl+O or drag and drop and it should work. Click the **Customize** button to change the word count, word separator etc.

If for some reason you cannot open the file in your web browser you can run a simple local web server to access it. Python3 is installed in most Linux and Unix systems these days, so you can just:

```sh
cd /to/this/directory
python3 -m http.server 3344
```

Then open `http://localhost:3344/dicemate.html` to use Dicemate.

As extra step of security, you can enable "Work Offline" mode on your browser if it supports it. On IceCat/Firefox/based browsers this is done with Alt+F -> Work Offline. On Iridium/Chromium/based browsers there are extensions to do this such as, [this one](https://github.com/Emano-Waldeck/work-offline/) (but beware of misbehaving extensions because it works only for web pages, not extensions).

If you are accessing this via a web URL, make sure it is coming over TLS/HTTPS (padlock present on location bar). Make sure your browser is from a legitimate source and does not have any suspicious addon/extension.

It should not access any URL on the web for any resources. Although it may request a favicon.ico if you use it through gitlab.io URL which is added automatically to HTML with `.gitlab-ci.yml`. The [favicon.ico file is created transparently](../assets/README.md) from an SVG file. If you are concerned, load the HTML file available on this directory instead, which does not request `favicon.ico` because it uses an [empty `base64` trick](https://www.faviconcodegenerator.com/prevent-favicon-404-error.php). If you want to check, right click anywhere on the page -> Inspect -> go to Network tab and refresh the page to see which resources are being accessed and from where.

Make sure to maintain [general precautions](../README.md#precautions) when using.

## Tested browsers

- IceCat, Firefox, LibreWolf
- Iridium
- Badwolf
- Epiphany (GNOME Web)
