# Dicemate Nim

This is the Dicemate's Nim implementation. It uses Cryptographically Secure Pseudo Random Number Generator to generate Diceware passphrases.

## Prerequisites

To use this you'll have to build from source code. Don't worry, this should be straight forward. Make sure you have [`nim` installed](https://nim-lang.org/install.html). It is probably already available in your package repo. Then:

```sh
cd /path/to/this/dir
git clone --depth 1 https://github.com/euantorano/sysrandom.nim.git
```

Then to build:

```sh
nim compile -d:release --opt:size dicemate.nim
# or to have debug symbols included (for devs and troubleshooting)
nim compile dicemate.nim
```

**NOTE:** You can also add `--run` to compile and run. But this is not necessary as you'll see that the binary is generated anyway and it is faster to run it directly. See Usage section below.

## Usage

The build command above should produce a binary named `dicemate` in the currect directory. In a typical Linux x86_64, the release binary is about 213kb in size. The default EFF wordlist is included in the binary.

So now you would be able to run:

```
./dicemate
```

Example:

```
$ ./dicemate -s='-'
someword-someword-someword-someword-someword-someword-someword
$ ./dicemate -a='*_' -z='_*' -s='-'
*_someword-someword-someword-someword-someword-someword-someword_*
$ ./dicemate -w=9
someword someword someword someword someword someword someword someword someword
```

**NOTE:** Dicemate Nim requires to put an equal sign (`=`) in between parameters and it's arguments. If you forget this, the output might not be what you expect.

Run `./dicemate -h` to see all options.

## Systemwide install

This is optional, but you can install Dicemate Nim like the following:

```sh
sudo cp dicemate /usr/local/bin/
```

This should let you access `dicemate` directly from terminal on any directory you are currently at:

```sh
dicemate -h
```
