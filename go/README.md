# Dicemate Go

This is the Go implementation of Dicemate. It uses CSPRNG to simulate random virtual dice rolls to generate Diceware passphrases.

## Prerequisites

This Go implementation requires to build from source code. But don't worry, it's not that hard. First check if you have `go` or `golang` on your operating system's package repo. If not, [install Go from it's website](https://golang.org/dl/).

Then to build, just:

```sh
cd /path/to/this/dir
go build -ldflags "-s -w" dicemate.go
# or to have debug symbols included, run this instead:
go build dicemate.go
```

There should be a `dicemate` binary created after this.

**NOTE:** You can also run `go run dicemate.go` to run without building. This is convenient, but should be slow. It is recommended to build it instead if you want to generate passphrases faster. Then check the Usage section below to learn how to use it.

## Usage

The build command above should produce a binary named `dicemate` in the currect directory. In a typical Linux x86_64, the release binary is about 1.6MiB in size. The default EFF wordlist is included in the binary.

So now you would be able to run:

```
./dicemate
```

Example:

```
$ ./dicemate -s='-'
someword-someword-someword-someword-someword-someword-someword
$ ./dicemate -a='*_' -z='_*' -s='-'
*_someword-someword-someword-someword-someword-someword-someword_*
$ ./dicemate -w=9
someword someword someword someword someword someword someword someword someword
```

**NOTE:** Dicemate Go requires to put an equal sign (`=`) in between parameters and it's arguments. If you forget this, the output might not be what you expect.

Run `./dicemate -h` to see all options.

## Systemwide install

This is optional, but you can install Dicemate Nim like the following:

```sh
sudo cp dicemate /usr/local/bin/
```

This should let you access `dicemate` directly from terminal on any directory you are currently at:

```sh
dicemate -h
```
