# Precautions


**WARNING:** Advices mentioned are not written by a security professional, so be sure to check them with one. If the safety of your life and wellbeing depends on a passphrase, seek professional security advice instead of solely depending on this document.


Like every other security software, security outcome is largely dependent on how it is used. This document has some cautionary notes that you can take into consideration when using Dicemate. To ensure you get the most security out of it, it is recommended that you read this document, if possible, before even using Dicemate.

Here are [some good ones](https://secure.research.vt.edu/diceware/#eff) to start with:

> You should ensure that:
> 
> - You [read about Diceware](https://world.std.com/~reinhold/diceware.html) and understand the strengths and limitations of this approach.
> - Don't generate passphrases on a machine you don't own and control. No public machines!
> - Make sure no one else is in the room with you that can 'shoulder surf'.
> - You close the browser window, when you are done.
> - You always visit this site over a TLS (HTTPS) connection. This is enforced with HSTS. A non-TLS HTTP connection opens you up to trivial man-in-the-middle attack on the code or the wordlists.
> - You are using the [latest version of a modern browser](https://www.whatismybrowser.com/).

The last three are appropriate for web browser based versions of Dicemate. HTTPS is not that important for localhost environments. The 'shoulder surf' point is appropriate for anything in this project that shows the passphrase on screen - close the window after use so that no one other than you sees the passphrase.

For browser based versions, private or incognito window can be used as a method of isolation and protecting your history of you accessing the file/URL. More on that later.

## Reducing trust

Do not trust this project blindly. Check the code yourself. It's written with simplicity in mind. So it should be fairly easy to read even if you have just a basic understanding of how programming languages work. Everything is divided in small, easy to understand functions. [Classes](https://simple.wikipedia.org/wiki/Class_(programming)) and complex code structures are avoided to ensure this characteristic. So read yourself and be certain that it's not doing anything fishy. If you don't understand anything, feel free to post an issue.

Try to reduce trust as much as possible. For example, when you run Dicemate from the gitlab.io URL, you have to trust GitLab, integrity of their setup, your ISP and probably many other parties listening in on your network. Even though the gitlab.io version does not transfer your passphrases over the internet, [an attacker might change the files you request](https://blog.heckel.io/2013/07/01/how-to-use-mitmproxy-to-read-and-modify-https-traffic-of-your-phone/) and make it do so without your knowledge.

You can, for example, use TOR Browser to download the project source code. When you'll access the source code through TOR without logging in anywhere, you'll just be another stranger on the internet and it will be difficult to track and target you. Then disconnect from internet and run the project offline to reduce trust on some of the other parties mentioned. This way you are only trusting the TOR exit node - who doesn't know who you are due to layers of encryption and just fetches the content for you.

## Passphrase uses

Diceware passphrases are good for protecting really important things that protect other important things, like a master key. People like to use one for their password manager, PGP/GPG key, full disk encryption key etc. Mainly because Diceware passphrases are comparatively easy to remember and still gets you a pretty good security out of it. Keeps things secure, keeps things simple - that's the main idea here.

If you have a password manager setup with a relatively easy to remember master password, you can generate and use more Diceware passphrases for websites/software you use. Some might restrict you by enforcing smaller password length. If you can't fit the whole passphrase, then do not use Dicemate and rather use some other reliable random password generator or try with a custom small-word wordlist.

Use a secure password manager, preferably offline and certainly encrypted, to save those passphrases. Remember to keep an encrypted (preferably offline) backup of those passphrases regularly.

Diceware passphrases are easy to remember, but it is fair to assume that you can't remember too many of these passphrases. It isn't possible unless you have a really good memory. This is one of its limitations. So create one good passphrase, remember it. Use it to protect your password manager data and store other data in that.

Do not re-use the same passphrase to protect multiple things. Because if one gets compromised, the other one gets compromised automatically. As suggested before, use a password manager and use unique passphrases for each software/website.

## Write passphrases down

You may think you'll remember the passphrase, but actually it's not guaranteed that you'll be able to, unless you've typed it hundreds of times and your brain got used to it. For a start, English and other languages have so many suffixes for words that it is easy to fog your memory. Your passphrase may have the word "loosely" but you may wrongly remember it as "loose" or "looser".

Computers will only accept the exact passphrase and not any variations so you'll be locked out for life from your encrypted storage with no practical way to get your data back. So write down your passphrase and keep it somewhere safe. Think of it as your very own "Forgot password" option. For insurance, keep another copy on a place far away from where you usually live. It is generally a bad idea to store your master passphrase digitally, even encrypted -- but may be a better choice depending on your threat model (e.g. if people surrounding you are nosy and find everything you hide).

## Do not change the words

Do not modify the passphrase yourself, especially the words. There has been [a research that humans are very bad at picking words themselves](http://www.jbonneau.com/doc/BS12-USEC-passphrase_linguistics.pdf). Let Dicemate do it for you. Ideally, use the suffix, prefix and separator settings to add things if you desire. Never mix words from multiple passphrases because as already told, humans are bad at randomizing stuff.

## Passphrase length

Select at least 7 words to be adequately crack-resistant. As [Micah Lee](https://theintercept.com/2015/03/26/passphrases-can-memorize-attackers-cant-guess/) explains:

> with two words, there are 7,776^2, or 60,466,176 different potential passphrases. On average, a two-word Diceware passphrase could be guessed after the first 30 million tries. And a five-word passphrase, which would have 7,776^5 possible passphrases, could be guessed after an average of 14 quintillion tries (a 14 with 18 zeroes).
> 
> ...
> 
> The amount of uncertainty in a passphrase (or in an encryption key, or in any other type of information) is measured in bits of entropy. You can measure how secure your random passphrase is by how many bits of entropy it contains. Each word from the Diceware list is worth about 12.92 bits of entropy (because 2^12.92 is about 7,776). So if you choose seven words you’ll end up with a passphrase with about 90.5 bits of entropy (because 12.92 times seven is about 90.5).
> 
> ...
> 
> At one trillion guesses per second — per Edward Snowden’s January 2013 warning — it would take an average of 27 million years to guess this passphrase.
> 
> ...
> 
> A five-word passphrase, in contrast, would be cracked in just under six months and a six-word passphrase would take 3,505 years, on average, at a trillion guesses a second. Keeping Moore’s Law in mind, computers are constantly getting more powerful, and before long 1 trillion guesses a second might start looking slow, so it’s good to give your passphrases some security breathing room.

This article is written in 2015 and "trillion guesses per second" estimate is 2 years older than that. So imagine how processing power have evolved since then. Keeping this in mind, 7 words is a good minimum threshold to start with.

## Wordlist

For wordlist, you can select any. Some implementations of Dicemate have the option to select a custom wordlist file. Just ensure that the words are unique and has 7776 words with proper numbering. There is a paragraph about this in the above article:

> With a system like this, it doesn’t matter at all that the word list you’re choosing from is public. It doesn’t even matter what the words in the list are (two-letter words are just as secure as six-letter words). All that matters is how long the list of words is and that each word on the list is unique. The probability of guessing a passphrase made of these randomly chosen words gets exponentially smaller with each word you add, and using this fact it’s possible to make passphrases that can never be guessed.

## Operating environment

Be wary of malware that may screenshot your passphrase. If you type your passphrases manually, be wary of keyloggers too. There could be browser extensions to steal generated passphrases shown in a web ui. So do not use Dicemate on a computer that others use. Absolutely do not use it on a public computer (like in a library or cyber cafe). Be cautious of people surrounding you looking at your screen. This may include people sitting or walking behind you or someone having access to a CCTV camera pointed at you. Do not let them see your passphrase.

Either an amnestic system that doesn't store anything, resets itself on each boot like [Tails](https://tails.boum.org/) or a full disk encrypted operating system is recommended to not jeopardize your passphrase to unwanted people. This will save you in case your computer or storage device got stolen or gotten wrongly disposed of.

## Storage

Do not neglect the storage device, e.g. hard disk, SSD, USB flash drive. Do not store your passphrase ever unencrypted on these, especially SSDs and flash drives. It cannot be erased (ever) from disk-less memory. Only way would be to destroy the drive. Hard disks with spinning disks might have a chance if you use [`shred`](https://www.linuxquestions.org/questions/linux-newbie-8/difference-between-the-rm-and-shred-in-linux-649436/#post3185393), but still not recommended. Classic pen and paper is recommended instead.

If you can't use a full disk encrypted operating system or an amnestic one, and you have to store the passphrase digitally, use at least EncFS. Even though [EncFS has some security concerns](https://en.wikipedia.org/wiki/EncFS#EncFS_1.8_security_concerns) it is still better than storing it in plain text. Read the previous two paragraphs to know why. You can more appropriately use [CryFS](https://www.cryfs.org) (which solves [many of the issues with EncFS](https://www.cryfs.org/comparison)) or [VeraCrypt](https://www.veracrypt.fr/en/Home.html). Do not use any archiving format with password, such as, .zip or .7z, because they require you to extract the file before reading, which will write the file data to your storage device on plain text. The other suggestions mentioned previously use a mounting system and never writes anything in plain text to storage device.

## Web safety

To not have to use cookies, Dicemate JS uses GET parameters to determine your parameters, so it is possible it will be recorded by the browser as history. If you use a Web UI, use Incognito/Private window or TOR Browser if not running on localhost. If you do not trust the web server running the other versions where passphrase generation happens server side, run them locally instead.

## Improving seed

To give CSPRNG a better chance, you can move your mouse, move around some windows, type something on keyboard, watch a video or do something CPU intensive in between generating passphrases. These will add random data from physical world to the seed which will help the randomization algorithm to generate more unpredictable random passphrases.

## Secrecy

Do not brag to anyone, especially anyone outside of your trust circle, that you use this project. This project may have its flaws and vulnerabilities. If they come to know that you use this project, they may get clues on how to attack your passphrase.

Best is to trust no one. Let this one thing be as secret as it can be. Do not even discuss anything around it. If someone presses you too much, show them how some other people manage their passphrases on the internet and let them do further research. e.g. [EFF's - Creating Strong Passwords page](https://ssd.eff.org/en/module/creating-strong-passwords) or [this Stack Exchange discussion](https://unix.stackexchange.com/questions/462/how-to-create-strong-passwords-in-linux).
