# Dicemate Python

Diceware implementation written in Python.

Don't forget to read [precautions](../README.md#precautions) in the project readme.

The EFF wordlist has been converted to `lines` variable values with a bash command:

```sh
sed -ne 's/^\(.*\)$/'\''\1'\'',/p' eff_large_wordlist.txt > python.bak
```

## Usage

Have [Python 3](https://www.python.org/) installed on the system. On most Unix/Linux systems it is as easy as installing the `python` or in some cases `python3` package. But usually Python 3 is already installed because so many packages require it.

Then run:

```sh
./dicemate.py
# or
python3 dicemate.py
```

Example:

```
$ ./dicemate.py -s '-'
someword-someword-someword-someword-someword-someword-someword
$ ./dicemate.py -a '*_' -z '_*' -s '-'
*_someword-someword-someword-someword-someword-someword-someword_*
$ ./dicemate.py -w 9
someword someword someword someword someword someword someword someword someword
```

Run `./dicemate.py -h` for CLI arguments.
