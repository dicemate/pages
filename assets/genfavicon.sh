#!/usr/bin/env bash

cd "$(dirname $0)"

if [ -z "$(command -v convert)" ]; then
	echo '"convert" binary is missing. Please install ImageMagick.'
	exit 1
fi

convert -resize x16 -gravity center -crop 16x16+0+0 dicemate-64.png -background transparent favicon.ico
