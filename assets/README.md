# Assets

This directory contains assets, esp. graphical ones, relating to the project.

- `dicemate-64.svg`: A 64x64 px Dicemate project logo vector image file.
- `dicemate-64.png`: A png export from `dicemate-64.svg` and then compressed with `compresspng.sh`
- `compresspng.sh`: A script to compress `dicemate-64.png`.
- `genfavicon.sh`: Script to generate `favicon.ico` from `dicemate-64.png`.
- `favicon.ico`: Icon generated from `dicemate-64.png` with `genfavicon.sh`. This file is later copied to `public/` output directory with `.gitlab-ci.yml`.
