#!/usr/bin/env bash

# Compresses PNG files to lower file sizes.
# License: CC0

# Usage:
#   - Make sure to chmod +x compresspng.sh
#   - ./compresspng.sh

# Ref: http://www.ffnn.nl/pages/articles/media/png-optimization.php

[ -z "$(command -v convert)" ] && echo 'Error: convert binary missing. Please install ImageMagick.' && exit 1
[ -z "$(command -v optipng)" ] && echo 'Error: optipng binary missing. Please install optipng.' && exit 2

cd "$(dirname $0)"

_input_dir="$(dirname $1)"
_input_filename="$(basename $1)"

# Convert to indexed color space
convert -quality 0 +dither -colors 256 "dicemate-64.png" "dicemate-64.png"

# Compress even more with optipng
optipng -zc1-9 -zm1-9 -zs0-2 -f0-5 -out "dicemate-64.png" "dicemate-64.png"
