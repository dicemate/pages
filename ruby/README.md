# Dicemate Ruby

This implementation of Dicemate is made with Ruby language.

If you run `ruby -v` and see a version being returned, it is installed. Please make sure that the version is 2.5.0 or later. This is for [security reasons](https://paragonie.com/blog/2016/05/how-generate-secure-random-numbers-in-various-programming-languages#ruby-csprng). If you don't see any version and instead see an error message, you will have to [install Ruby](https://www.ruby-lang.org/en/documentation/installation/) version 2.5.0 or later (latest one is recommended).

Then:

```sh
./dicemate.rb
# or
ruby dicemate.rb
```

**NOTE:** The first command needs executable permission. If it complains permission denied or something, run `chmod +x dicemate.rb`.

Optionally, you can pass command line parameters to customize the passphrase.

Examples:

```sh
$ ./dicemate.rb
someword someword someword someword someword someword someword
$ ./dicemate.rb -w 9 -s '/'
someword/someword/someword/someword/someword/someword/someword/someword/someword
$ ./dicemate.rb -a '_*' -z '*_' -s '-'
_*someword-someword-someword-someword-someword-someword-someword*_
```

Run `./dicemate.rb -h` for more details and options.

Be sure to read and follow [precautions](../README.md#precautions) when using.
